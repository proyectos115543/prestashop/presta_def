<?php
/* Smarty version 3.1.48, created on 2023-07-19 18:29:19
  from '/Users/martu/Sites/localhost/Proyectos/Prestashop/presta_def/modules/welcome/views/templates/tooltip.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.48',
  'unifunc' => 'content_64b80f5f996fd7_75725054',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd551159078195493b1ef757ea8b322aa36fe9b3a' => 
    array (
      0 => '/Users/martu/Sites/localhost/Proyectos/Prestashop/presta_def/modules/welcome/views/templates/tooltip.tpl',
      1 => 1676360366,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_64b80f5f996fd7_75725054 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="onboarding-tooltip">
  <div class="content"></div>
  <div class="onboarding-tooltipsteps">
    <div class="total"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Step','d'=>'Modules.Welcome.Admin'),$_smarty_tpl ) );?>
 <span class="count">1/5</span></div>
    <div class="bulls">
    </div>
  </div>
  <button class="btn btn-primary btn-xs onboarding-button-next"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Next','d'=>'Modules.Welcome.Admin'),$_smarty_tpl ) );?>
</button>
</div>
<?php }
}
