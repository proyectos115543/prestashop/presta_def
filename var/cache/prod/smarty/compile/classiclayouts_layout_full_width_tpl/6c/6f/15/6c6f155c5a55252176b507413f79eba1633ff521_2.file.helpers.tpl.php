<?php
/* Smarty version 3.1.48, created on 2023-07-19 18:29:34
  from '/Users/martu/Sites/localhost/Proyectos/Prestashop/presta_def/themes/classic/templates/_partials/helpers.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.48',
  'unifunc' => 'content_64b80f6e8a2481_99783746',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6c6f155c5a55252176b507413f79eba1633ff521' => 
    array (
      0 => '/Users/martu/Sites/localhost/Proyectos/Prestashop/presta_def/themes/classic/templates/_partials/helpers.tpl',
      1 => 1681999934,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_64b80f6e8a2481_99783746 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/Users/martu/Sites/localhost/Proyectos/Prestashop/presta_def/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/6c/6f/15/6c6f155c5a55252176b507413f79eba1633ff521_2.file.helpers.tpl.php',
    'uid' => '6c6f155c5a55252176b507413f79eba1633ff521',
    'call_name' => 'smarty_template_function_renderLogo_51466348464b80f6e89ad84_87554683',
  ),
));
?> 

<?php }
/* smarty_template_function_renderLogo_51466348464b80f6e89ad84_87554683 */
if (!function_exists('smarty_template_function_renderLogo_51466348464b80f6e89ad84_87554683')) {
function smarty_template_function_renderLogo_51466348464b80f6e89ad84_87554683(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
    <img
      class="logo img-fluid"
      src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
"
      alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
      width="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
"
      height="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
">
  </a>
<?php
}}
/*/ smarty_template_function_renderLogo_51466348464b80f6e89ad84_87554683 */
}
